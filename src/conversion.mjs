import Jimp from 'jimp';

const resizeImage = (filePath, width, height) => {
    return new Promise(resolve => {
		console.log('reding ', filePath)
        Jimp.read({url: filePath}, (err, lenna) => {
            if (err) throw err;
            const originalWidth = lenna.bitmap.width
            const originalHeight = lenna.bitmap.height

            const originalRatio = originalWidth / originalHeight
            const desiredRatio = width / height

            let resizeWidth = 0
            let resizeHeight = 0

            if (originalRatio < desiredRatio) {
                resizeWidth = width
                resizeHeight = Jimp.AUTO
            } else {
                resizeWidth = Jimp.AUTO
                resizeHeight = height
            }

            lenna.resize(resizeWidth, resizeHeight).quality(65).getBufferAsync('image/jpeg').then(buffer => {
                resolve(buffer)
            })
        });
    })
}

/**
 * @type {(sourceUrl: string, filename: string, uploadFile: (filename: string, buffer: Buffer, _: {contentType: string}) => Promise<void>) => Promise<void>}
 */
export const convert = async (sourceUrl, filename, uploadFile) => {
	console.log(`Converting url: ${sourceUrl}`)

	const filenameS = filename.replace(/^(.*)(\.[a-z]+)$/, '$1_s$2')
	console.log('filenameS ', filenameS)
	const bufferS = await resizeImage(sourceUrl, 455, 320)
	await uploadFile(filenameS, bufferS, {contentType: 'image/jpeg'})


	const filenameL = filename.replace(/^(.*)(\.[a-z]+)$/, '$1_l$2')
	console.log('filenameL ', filenameL)
	const bufferL = await resizeImage(file.tmpPath, 1000, 500)
	await uploadFile(filenameL, bufferL, {contentType: 'image/jpeg'})
}